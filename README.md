This is the repo for Compiler course.

Name 	    : Sameer Dhiman

Roll No.  : 111801038

# Instructions

Source for the _tiger_ compiler is in the **src** directory. Run make inside that directory to compile the compilers and get **tc** executable.

## Usage

```
./tc <file> --pp : for compiling the tiger program and getting a pretty print ourput
./tc <file> --ast : for compiling the tiger program and getting the ast as output
./tc <file> -m : for getting a compiled output in mips assembly (Not yet implemented)
```

# Reference
[Tiger Project](https://www.lrde.epita.fr/~tiger/tiger.html)

[Modern Compiler in ML](https://www.cs.princeton.edu/~appel/modern/ml/)
