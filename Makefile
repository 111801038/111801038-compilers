TIGER= tiger/ast.sml tiger/tiger.grm.sml tiger/tiger.lex.sml 
TARGET = target/pp.sml target/printAbsyn.sml target/translate.sml target/printTree.sml
IR = ir/tree.sml ir/temp.sml ir/canon.sml
TIGERDIR= tiger/
TARGETDIR = target/
IRDIR = ir/

${TIGERDIR}%.lex.sml: ${TIGERDIR}%.lex
	mllex $<

${TIGERDIR}%.grm.sml: ${TIGERDIR}%.grm
	mlyacc $<

all: tc

.PHONY: all clean test

clean:
	rm -f ${DIR}*.grm.sml ${DIR}*.grm.desc ${DIR}*.grm.sig ${DIR}*.lex.sml tc

tc: tc.sml tc.mlb ${TARGET} ${TIGER} ${IR}
	mlton tc.mlb

ast: all
	${CURDIR}/tc test.tg --ast

test: all
	${CURDIR}/tc test.tg --pp
