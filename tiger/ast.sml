structure Tiger = struct

type id = string
type typeId = id

datatype BinOp = DIV
               | MUL
               | ADD
               | SUB
               | LE
               | GE
               | L
               | G
               | EQ
               | AND
               | OR
               | NEQ

datatype Keyword = ARRAY
                 | IF
                 | ELSE
                 | THEN
                 | WHILE
                 | FOR
                 | TO
                 | DO
                 | LET
                 | IN
                 | END
                 | OF
                 | BREAK
                 | NIL
                 | FUNCTION
                 | VAR
                 | TYPE
                 | IMPORT
                 | PREMITIVE


datatype lValue = V of id
                | B of lValue * exp
                | D of lValue * id

     and dec = TypeDec of { name: id,
                            ty: ty }

             | VarDec of {  name: id,
                            ty: typeId option,
                            init: exp }

             | FuncDec of { name: id,
                            fields: tyfield list,
                            ty: typeId option,
                            body: exp }
             | PrimDec of { name: id,
                         fields: tyfield list,
                         ty: typeId option }

     and ty = AliasTy of typeId
            | RecTy of tyfield list
            | ArrTy of typeId

     and tyfield = TyField of { name: id,
                                 ty: typeId }

     and exp = Nil
             | Int of int
             | Str of string
                   (* Array and Record *)
             | Array of { size: exp,
                          ty: typeId,
                          init: exp }
             | Record of { ty: typeId,
                           field: (id * exp) list }
                    (* variables *)
             | Var of lValue

             | FunCall of { func: id,
                            args: exp list }

             | Opr of { x: exp,
                        opr: BinOp,
                        y: exp }
             | Br of exp list
             | Neg of exp

             | Ass of { x: lValue,
                        y: exp }

             | If of { cond: exp,
                       body: exp,
                       alt: exp option }
             | While of { cond: exp,
                          body: exp }

             | For of { var: id,
                        from: exp,
                        to: exp,
                        body: exp }
             | Break
             | Let of { decs: dec list,
                        body: exp list }

datatype prog = E of exp


end
