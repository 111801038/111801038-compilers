type lineNo            = int
type pos               = lineNo
val  lineRef : pos ref = ref 0  

fun updateLine n      = lineRef := !(lineRef) + n

type svalue        = Tokens.svalue
type ('a,'b) token = ('a,'b) Tokens.token
type lexresult     = (svalue,pos) token

fun lineRange l r = "line " ^ l

fun error (e,l,r) = TextIO.output(TextIO.stdErr, lineRange l r ^ ":" ^ e ^ "\n")

fun eof   ()      = Tokens.EOF (!lineRef,!lineRef)

fun stringClean s = String.substring(s, 1, size s - 2) 

val insideComment = ref 0;

val insideString = ref 0;

val stringBegin = ref 0;

val s = ref "";


%% 
%header (functor TigerLexFun(structure Tokens: Tiger_TOKENS));
%s COMMENT STRING;
%%

<INITIAL> "/*"      => ( insideComment := !insideComment + 1; YYBEGIN COMMENT; continue() );

<COMMENT> "/*"      => ( insideComment := !insideComment + 1; continue() );
<COMMENT> "*/"      => ( insideComment := !insideComment - 1; if (!insideComment = 0) then (YYBEGIN INITIAL; continue()) else continue() );
<COMMENT> . | [\n]  => ( updateLine 1; continue() );




<INITIAL> \n	=> (updateLine 1; continue());
<INITIAL> [\ \t]+ => (continue());
<INITIAL> "type" => (Tokens.TYPE(!lineRef, !lineRef+4));
<INITIAL> "var"  => (Tokens.VAR(!lineRef, !lineRef+3));
<INITIAL> "function"  => (Tokens.FUNCTION(!lineRef, !lineRef+8));
<INITIAL> "primitive"  => (Tokens.PRIMITIVE(!lineRef, !lineRef+9));
<INITIAL> "break" => (Tokens.BREAK(!lineRef, !lineRef+5));
<INITIAL> "of" => (Tokens.OF(!lineRef, !lineRef+2));
<INITIAL> "end" => (Tokens.END(!lineRef, !lineRef+3));
<INITIAL> "in" => (Tokens.IN(!lineRef, !lineRef+2));
<INITIAL> "nil" => (Tokens.NIL(!lineRef, !lineRef+3));
<INITIAL> "let" => (Tokens.LET(!lineRef, !lineRef+3));
<INITIAL> "do" => (Tokens.DO(!lineRef, !lineRef+2));
<INITIAL> "to" => (Tokens.TO(!lineRef, !lineRef+2));
<INITIAL> "for" => (Tokens.FOR(!lineRef, !lineRef+3));
<INITIAL> "while" => (Tokens.WHILE(!lineRef, !lineRef+5));
<INITIAL> "else" => (Tokens.ELSE(!lineRef, !lineRef+4));
<INITIAL> "then" => (Tokens.THEN(!lineRef, !lineRef+4));
<INITIAL> "if" => (Tokens.IF(!lineRef, !lineRef+2));
<INITIAL> "array" => (Tokens.ARRAY(!lineRef, !lineRef+5));
<INITIAL> ":=" => (Tokens.ASSIGN(!lineRef, !lineRef+2));
<INITIAL> "|" => (Tokens.OR(!lineRef, !lineRef+1));
<INITIAL> "&" => (Tokens.AND(!lineRef, !lineRef+1));
<INITIAL> ">=" => (Tokens.GE(!lineRef, !lineRef+2));
<INITIAL> ">" => (Tokens.G(!lineRef, !lineRef+1));
<INITIAL> "<=" => (Tokens.LE(!lineRef, !lineRef+2));
<INITIAL> "<" => (Tokens.L(!lineRef, !lineRef+1));
<INITIAL> "<>" => (Tokens.NEQ(!lineRef, !lineRef+2));
<INITIAL> "=" => (Tokens.EQ(!lineRef, !lineRef+1));
<INITIAL> "/" => (Tokens.DIV(!lineRef, !lineRef+1));
<INITIAL> "*" => (Tokens.MUL(!lineRef, !lineRef+1));
<INITIAL> "-" => (Tokens.SUB(!lineRef, !lineRef+1));
<INITIAL> "+" => (Tokens.ADD(!lineRef, !lineRef+1));
<INITIAL> "." => (Tokens.DOT(!lineRef, !lineRef+1));
<INITIAL> "}" => (Tokens.RPAREN(!lineRef, !lineRef+1));
<INITIAL> "{" => (Tokens.LPAREN(!lineRef, !lineRef+1));
<INITIAL> "]" => (Tokens.RSBRACK(!lineRef, !lineRef+1));
<INITIAL> "[" => (Tokens.LSBRACK(!lineRef, !lineRef+1));
<INITIAL> ")" => (Tokens.RBRACK(!lineRef, !lineRef+1));
<INITIAL> "(" => (Tokens.LBRACK(!lineRef, !lineRef+1));
<INITIAL> ";" => (Tokens.SEMICOLON(!lineRef, !lineRef+1));
<INITIAL> ":" => (Tokens.COLON(!lineRef, !lineRef+1));
<INITIAL> "," => (Tokens.COMMA(!lineRef, !lineRef+1));
<INITIAL> [0-9]* => (Tokens.INT(valOf(Int.fromString(yytext)), !lineRef, !lineRef+size yytext));
<INITIAL> [a-zA-Z][a-zA-Z0-9_]* => (Tokens.ID(yytext, !lineRef, !lineRef+size yytext));


<INITIAL>\" => (YYBEGIN STRING; stringBegin := yypos; insideString := 1;  s := ""; continue());
<STRING>\\n => (s := !s ^ "\n"; continue());
<STRING>\\t => (s := !s ^ "\t"; continue());
<STRING>\\\" => (s := !s ^ "\""; continue());
<STRING>\\\\ => (s := !s ^ "\\"; continue());
<STRING>\\a => (s := !s ^ "\a"; continue());
<STRING>\\b => (s := !s ^ "\b"; continue());
<STRING>\\f => (s := !s ^ "\f"; continue());
<STRING>\\r => (s := !s ^ "\r"; continue());
<STRING>\\v => (s := !s ^ "\v"; continue());
<STRING> [^\\"\n] => (s := !s ^ yytext; continue());

<STRING>\" => (insideString := 0; YYBEGIN INITIAL; Tokens.STRING(!s, !stringBegin, yypos));
<STRING>\n => (error("Illegal character in String", Int.toString(!lineRef), yypos); continue());
<INITIAL>. => (error("Illegal character", Int.toString(!lineRef), yypos); continue());