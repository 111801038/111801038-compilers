signature TEMP =
  sig
     type label = int
     type temp = int
     val newlabel : unit -> label
     val newtemp  : unit -> temp
     val curtemp : unit -> temp
     val curlabel : unit -> label
  end

structure Temp :> TEMP = struct


   type label = int
   type temp  = int

   val nextLabel = ref 0
   val nextTemp  = ref 0

   fun newlabel _ = ( nextLabel := !nextLabel + 1; !nextLabel )
   fun newtemp _ = ( nextTemp := !nextTemp + 1; !nextTemp )

   fun curlabel _ = !nextLabel
   fun curtemp _ = !nextTemp
end
