signature CANON = 
sig

    val linearize : Tree.stmt -> Tree.stmt list

    val basicBlocks : Tree.stmt list -> (Tree.stmt list list * Temp.label)

    val traceSchedule : Tree.stmt list list * Temp.label -> Tree.stmt list

    val canon : Tree.expr -> Tree.stmt list

end

structure Canon : CANON =
struct

    fun commute(Tree.EXP(Tree.CONST _), _)  = true
        | commute(_, Tree.NAME _)           = true
        | commute(_, Tree.CONST _)          = true
        | commute _                         = false

    fun reorder ((Tree.CALL e) :: es)   = reorder(Tree.ESEQ(Tree.MOVE(Tree.TEMP (Temp.newtemp()), Tree.CALL(e)), Tree.TEMP (Temp.curtemp())) :: es)
        | reorder (a :: rest)           = let
                                            val (stms, e) = do_exp a
	                                        val (stms2, es) = reorder rest
	                                    in
                                            if commute(stms2,e) then
                                                (Tree.SEQ(stms, stms2) , e::es)
	                                        else
                                                (Tree.SEQ( ( Tree.SEQ ( stms, Tree.MOVE(Tree.TEMP (Temp.newtemp()), e) ) ) , stms2), Tree.TEMP (Temp.curtemp()) :: es)
                                        end
        | reorder nil                   = (Tree.EXP(Tree.CONST 0), nil)


    and reorder_exp(es, build) = let
                                    val (stms, es2) = reorder es
                                in 
                                    (stms, build es2)
                                end

    and reorder_stm(es, build) = let
                                    val (stms, es2) = reorder es
                                in
                                    Tree.SEQ(stms, (build es2))
                                end
        
    and do_stm(Tree.SEQ(a, b))              = Tree.SEQ((do_stm a), (do_stm b))
        | do_stm(Tree.JUMP(e, labels))      = reorder_stm([e], fn [e] => Tree.JUMP(e, labels))
        | do_stm(Tree.CJUMP(opr, e1, e2, l1, l2)) = reorder_stm([e1, e2], fn [e1,e2] => Tree.CJUMP(opr, e1, e2, l1, l2))
        | do_stm(Tree.MOVE(Tree.TEMP t, Tree.CALL(e, es))) = reorder_stm(e::es, fn e::es => Tree.MOVE(Tree.TEMP t, Tree.CALL(e, es)))
        | do_stm(Tree.MOVE(Tree.TEMP t, b)) = reorder_stm([b], fn [b] => Tree.MOVE(Tree.TEMP t, b))
        | do_stm(Tree.MOVE(Tree.MEM e, b))  = reorder_stm([e, b], fn[e, b] => Tree.MOVE(Tree.MEM e, b))
        | do_stm(Tree.MOVE(Tree.ESEQ(s, e), b)) = do_stm(Tree.SEQ(s, Tree.MOVE(e,b)))
        | do_stm(Tree.EXP(Tree.CALL(e,es))) = reorder_stm(e::es, fn e::es => Tree.EXP(Tree.CALL(e,es)))
        | do_stm(Tree.EXP e)                = reorder_stm([e], fn [e] => Tree.EXP e)
        | do_stm s                          = reorder_stm([], fn [] => s)

    
    and do_exp(Tree.BINOP(opr, a, b))   = reorder_exp([a,b], fn [a,b] => Tree.BINOP(opr,a,b))
        | do_exp(Tree.MEM(a))           = reorder_exp([a], fn [a] => Tree.MEM(a))
        | do_exp(Tree.ESEQ(s, e))       = let
                                            val (stms, e) = do_exp e
                                        in
                                            (Tree.SEQ((do_stm s), stms), e)
                                        end
        | do_exp(Tree.CALL(e, es))      = reorder_exp(e::es, fn e::es => Tree.CALL(e,es))
        | do_exp e                      = reorder_exp([],fn _ => e)


    fun linear(Tree.SEQ(a, b), l)   = linear(a, linear(b, l))
        | linear(s,l)               = s::l

    fun linearize(stm0: Tree.stmt) : Tree.stmt list = linear(do_stm stm0, nil)

    fun nextBlock(((Tree.JUMP j))::rest, thisblock, blockList) = createBlocks(rest,  (rev ((Tree.JUMP j) :: thisblock) :: blockList))
        | nextBlock(((Tree.CJUMP c))::rest, thisblock, blockList) =  createBlocks(rest,  (rev ((Tree.CJUMP c) :: thisblock) :: blockList))
        | nextBlock( (Tree.LABEL l :: stms), thisblock, blockList) = nextBlock(Tree.JUMP(Tree.NAME l,[l]) :: (Tree.LABEL l :: stms), thisblock, blockList)
        | nextBlock(s::rest, thisblock, blockList) = nextBlock(rest, s::thisblock, blockList)
        | nextBlock(nil, thisblock, blockList) = nextBlock([Tree.JUMP(Tree.NAME (Temp.newlabel()), [(Temp.curlabel())])], thisblock, blockList)
    
    and createBlocks((Tree.LABEL l) :: tail, blockList) = nextBlock(tail, [(Tree.LABEL l)], blockList)
        | createBlocks(nil, blockList) = rev blockList
        | createBlocks(stms, blockList) = createBlocks(Tree.LABEL(Temp.newlabel())::stms, blockList)
    
    fun basicBlocks stms = (createBlocks(stms,nil),  Temp.curlabel())

    fun formMap(((currLabel as (Tree.LABEL s :: _)) :: xs), map) = formMap(xs, IntBinaryMap.insert(map, s, currLabel))
        | formMap([], map) = map
        | formMap _ = raise Fail "Somethin went wrong"

    fun getLast [x] = (nil, x) 
        | getLast(x :: xs) = (rev xs, x)
        | getLast _ = raise Fail "Something went wrong"

    fun traceHelper(map, currLabel, (first, Tree.JUMP(Tree.NAME lab, _)), rest) =  
            (case IntBinaryMap.find(map, lab) of
                  SOME(b2 as _::_) => first @ trace(map, b2, rest)
                | _ => currLabel @ nextLabel(map, rest))
        
        | traceHelper(map, currLabel, (first, Tree.CJUMP(opr,x,y,t,f)), rest) =
             (case (IntBinaryMap.find(map, t), IntBinaryMap.find(map, f)) of
                (_, SOME(b2 as _::_)) => currLabel @ trace(map, b2, rest)
                | (SOME(b2 as _::_), _) => first @ [Tree.CJUMP(opr,x,y,f,t)] @ trace(map, b2, rest)
                | _ =>  first @ [Tree.CJUMP(opr, x, y, t, Temp.newlabel()),	Tree.LABEL (Temp.curlabel()), Tree.JUMP(Tree.NAME f,[f])] @ nextLabel(map, rest))
        
        | traceHelper(map, currLabel, (first, Tree.JUMP _), rest) = currLabel @ nextLabel(map, rest)
        
        | traceHelper _ = raise Fail "Someting went wrong"


    and trace(map, currLabel as (Tree.LABEL lab :: _), rest) = 
            let
                val map = IntBinaryMap.insert(map, lab, nil)
            in
                traceHelper(map, currLabel, getLast (rev currLabel), rest)
            end
        | trace _ = raise Fail "Someting went wrong"

    and nextLabel(map, (currLabel as (Tree.LABEL lab :: _)) :: rest) =  (case IntBinaryMap.find(map, lab) of
                                                                            SOME(_::_) => trace(map, currLabel, rest)
                                                                            | _ => nextLabel(map, rest))
        | nextLabel(map, nil) = nil
        | nextLabel _ = raise Fail "Someting went wrong"

    fun traceSchedule(blocks, done) = nextLabel(formMap(blocks, IntBinaryMap.empty), blocks) @ [Tree.LABEL done]

    fun cleanExp [] = []
    |   cleanExp [x] =
                    (case x of 
                        Tree.EXP(Tree.CONST 0) => []
                    |   _ => [x])
    |   cleanExp (x :: xs) = (cleanExp [x]) @ (cleanExp xs)


    fun canon3 e = cleanExp(linearize(Tree.EXP(e)))
    fun canon2 e = basicBlocks(canon3 e)
    fun canon e = traceSchedule(canon2(e))

end