structure EC =
struct


structure TigerLrVals = TigerLrValsFun(structure Token = LrParser.Token)
structure TigerLex    = TigerLexFun(structure Tokens = TigerLrVals.Tokens)
structure TigerParser = Join( structure ParserData = TigerLrVals.ParserData
			     structure Lex        = TigerLex
			     structure LrParser   = LrParser
			   )

fun makeTigerLexer strm = TigerParser.makeLexer (fn n => TextIO.inputN(strm,n))
val makeFileLexer      = makeTigerLexer o TextIO.openIn

val arg = ref ""

val thisLexer = case CommandLine.arguments() of
		    []  => makeTigerLexer TextIO.stdIn
		 |  [x] => makeFileLexer x
		 |  [x, y] => if (String.isPrefix "-" y) then ( arg := y; makeFileLexer x )
		 			else (arg := x; makeFileLexer y)
		 | 	 _	=> (TextIO.output(TextIO.stdErr, "usage tc <file> --pp/--ast : for pretty print/ast\n"); OS.Process.exit OS.Process.failure)
		 
fun print_error (s,i:int,_) = TextIO.output(TextIO.stdErr,
					    "Error, line " ^ (Int.toString i) ^ ", " ^ s ^ "\n")

val (program,_) = TigerParser.parse (0,thisLexer,print_error,()) (* parsing *)
val _ = if !arg = "--pp" then
 	        print(PP.compile program)
        else if !arg = "-m" then
			print "Not yet implemented\n"
        else if !arg = "--ast" then
			PrintAbsyn.print (TextIO.stdOut, program)
        else if !arg = "--ir" then
			( PrintTree.compile(Translate.help program); print "\n")
		else if !arg = "--can" then
			( PrintTree.stmtListCompile(Canon.canon(Translate.help program)); print "\n"; PrintTree.framePrint 1)
		else
			print "usage tc <file> --pp/--ast : for pretty print/ast\n"

end
