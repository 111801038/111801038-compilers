structure PrintAbsyn : 
     sig val print : TextIO.outstream * Tiger.prog -> unit end =
struct 

structure A = Tiger

fun print (outstream, e0) =
 let fun say s =  TextIO.output(outstream,s)
  fun sayln s= (say s; say "\n") 

  fun indent 0 = ()
    | indent i = (say "  "; indent(i-1))

  fun opname A.ADD = "ADD"
    | opname A.SUB = "SUB"
    | opname A.MUL = "MUL"
    | opname A.DIV = "DIV"
    | opname A.EQ =  "EQ"
    | opname A.NEQ = "NEG"
    | opname A.L = "L"
    | opname A.LE = "LE"
    | opname A.G = "G"
    | opname A.GE = "GE"
    | opname A.AND = "AND"
    | opname A.OR = "OR"

  fun doExps d f [a] = (indent (d+1); f(a,d+1); sayln "")
    | doExps d f (a::r) = (indent (d+1); f(a,d+1); sayln ","; doExps d f r )
    | doExps d f nil = ()

  fun doPara d f [a] = (f(a,d+1))
    | doPara d f (a::r) = (say ""; f(a,d+1); say ", "; doPara d f r)
    | doPara d f nil = ()

  fun doDecs d f [a] = (indent (d+1); f(a,d+1))
    | doDecs d f (a::r) = (indent (d+1); f(a,d+1); doDecs d f r)
    | doDecs d f nil = ()


  fun linit(A.V(s),d) = (
                          say("V(" ^ s ^ ")")
                        )

    | linit(A.B(x,e),d) = (
                            say "B(";
                            linit(x, d);
                            say ", ";
                            exp(e, d);
                            say ")"
                          )
    
    | linit(A.D(x,e),d) = (
                            say "D(";
                            linit(x, d);
                            say( ", " ^ e ^ ")")
                          )

  and exp(A.Var x, d) = linit(x,d)

    | exp(A.Nil, d) = say "Nil"

    | exp(A.Int i, d) = (
                          say ("Int(" ^ (Int.toString i) ^ ")")
                        )

    | exp(A.Str(s),d) = (
                          say ("Str(" ^ "\"" ^ concat( map Char.toString (explode s) )^ "\"" ^ ")")
                        )

    | exp(A.FunCall{args, func},d) =  (
                                        say "FuncCall{ func : ";
                                        say(func);
                                        say ", args : ";
                                        doPara d exp args;
                                        say "}"
                                      )

    | exp(A.Array{ty, init, size},d) =  (
                                          say "Array{ ty : ";
                                          say (ty);
                                          say ", size : ";
                                          exp(size, d); say ", init : ";
                                          exp(init, d);
                                          say "}"
                                        )


    | exp(A.Record{field,ty},d) = let
                                    fun f((name,e),d) = (
                                                          say(name);
                                                          say (" = ");
                                                          exp(e,d)
                                                        )
	                                in 
                                    say "Record{ ty : ";
                                    say(ty);
                                    say ", field : { ";
                                    doPara 0 f field;
                                    say "}}" 
	                                end

    | exp(A.Neg(e), d) =  (
                            say "Neg(";
                            exp(e, d);
                            say ")"
                          )

    | exp(A.Opr{x,opr,y},d) = (
                                say "Opr{ x : ";
                                exp(x, d);
                                say ", opr : ";
                                say(opname opr);
                                say ", y : "; exp(y, d);
                                say "}"
                              )

    | exp(A.Br l, d) =  (
                          sayln "Br(";
                          doExps d exp l;
                          indent d;
                          say ")"
                        )

    | exp(A.Ass{x,y},d) = (
                            say "Ass{ x : ";
                            linit(x,d);
                            say ", y : ";
                            exp(y,d); say "}"
                          )

    | exp(A.If{cond,body,alt},d) =  (
                                      sayln "If{";
                                      indent d;
                                      say "cond : ";
                                      exp(cond, d); sayln ",";
                                      indent (d);
                                      sayln ("body : ");
                                      indent (d+1);
                                      exp(body, d+1);
                                      case alt of NONE => ()
                                                | SOME e => (
                                                              sayln ",";
                                                              indent d;
                                                              sayln ("alt : ");
                                                              indent (d + 1); exp(e,d+1)
                                                            );
                                      say "}"
                                    )

    | exp(A.While{cond,body},d) = (
                                    sayln ("While{");
                                    indent d;
                                    say "cond : ";
                                    exp(cond, d); sayln "";
                                    indent (d);
                                    sayln ("body : ");
                                    indent (d+1);
                                    exp( body,d+1);
                                    say "}"
                                  )

    | exp(A.For{var=x,from=lo,to=hi,body=body},d) = (
                                                      sayln "For{";
                                                      indent d;
                                                      say "var : ";
                                                      say(x);
                                                      say ", from : ";
                                                      exp(lo,d);
                                                      say (", to : ");
                                                      exp(hi,d);
                                                      sayln ",";
                                                      indent d;
                                                      sayln ("body : ");
                                                      indent (d+1);
                                                      exp(body,d+1)
                                                    )

    | exp(A.Break, d) = ( 
                          say ("Break")
                        )

    | exp(A.Let{decs,body},d) = (
                                  sayln "Let{"; indent (d); sayln "decs :";
                                  doDecs d dec decs;
                                  indent (d);
                                  sayln ("body :");
                                  doExps d exp body;
                                  say ("}")
                                )

  and dec(A.TypeDec l, d) = let 
                              fun tdec({name, ty}) =  (
                                                        say ("TypDec{ name : ");
                                                        say(name);
                                                        say (", ty : ");
                                                        typ(ty,d)
                                                      )  
                            in 
                              tdec(l);
                              sayln "}"
                            end

    | dec(A.VarDec{name,ty,init},d) = (
                                        say ("VarDec{ name : ");
                                        say(name);
                                        case ty of NONE => () 
                                                | SOME(s)=> (
                                                              say ", ty : ";
                                                              say(s)
                                                            );
                                        say (", init : ");
                                        exp(init,d);
                                        sayln "}"
                                      )

    | dec(A.FuncDec l, d) = let
                              fun f({name,fields,ty,body},d) =  (
                                                                  indent d;
                                                                  sayln ("name : " ^ name ^ ",");
                                                                  indent d;
                                                                  say "fields : ";
                                                                  doPara d tyfields fields;
                                                                  sayln ",";
		                                                              case ty of NONE => say ""
                                                                          | SOME(s) =>  (
                                                                                          indent d;
                                                                                          say "ty : ";
                                                                                          sayln (s ^ ",")
                                                                                        );
		                                                              indent d;
                                                                  sayln ("body : ");
                                                                  indent (d+1);
                                                                  exp(body, d+1)
                                                                )
	                          in
                              sayln ("FuncDec{ ");
                              f(l, d);
                              sayln "";
                              indent d;
                              sayln "}"
	                          end

    | dec(A.PrimDec{name, fields,ty},d) = (
                                            say ("PrimeDec{ name : ");
                                            say (name);
                                            say ", fields : ";
                                            doPara d tyfields fields;
                                            case ty of NONE => ()
                                                    | SOME(s) =>  (
                                                                    say ", ty : ";
                                                                    say (s)
                                                                  );
                                            sayln "}"
                                          )



 and  typ(A.AliasTy(s), d) =  (
                                say ("AliasTy(" ^ s ^ ")")
                              )

    | typ(A.RecTy l, d) = (
                            say "{ ";
                            doPara d tyfields l;
                            say "}"
                          )
		
    | typ(A.ArrTy(s),d) = (
                            say ("ArrTy(");
                            say (s ^ ")")
                          )

 and tyfields(A.TyField l , d) =  let
                                    fun f({name,ty},d) =  (
                                                            say ( "name : " ^ name );
                                                            say ", ty : "; say (ty)
                                                          )
                                  in
                                    say "TyField{ "; 
                                    f(l,d);
                                    say "}"
                                  end

 and program(A.E e, d) = (exp(e, d))  
 
 in  program(e0,0); sayln ""; TextIO.flushOut outstream

 end                            

end
