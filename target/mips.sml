structure MIPS = struct

datatype regs = Zero
              | At
              | V0
              | V1
              | A0
              | A1
              | A2
              | A3
              | T0
              | T1
              | T2
              | T3
              | T4
              | T5
              | T6
              | T7
              | S0
              | S1
              | S2
              | S3
              | S4
              | S5
              | S6
              | S7
              | T8
              | T9
              | K0
              | K1
              | Gp
              | Sp
              | Fp
              | Ra
              | BadVAddr
              | Status
              | Cause
              | EPC

datatype ('l, 't) inst = PrintInt of 't
                       | PrintString of 't
                       | ReadInt
                       | ReadString of 't * 't
                       | Exit

                       (* Arithematic and Logical Operations*)

                       | Add of 't * 't * 't
                       | AddI of 't * 't * 't
                       | AddU of 't * 't * 't
                       | AddIU of 't * 't * 't

                       | And of 't * 't * 't
                       | AndI of 't * 't * 't

                       | Div of 't * 't
                       | DivU of 't * 't
                       | Div2 of 't * 't *'t
                       | DivU2 of 't *'t *'t

                       | Mul of 't * 't * 't
                       | MulO of 't * 't * 't
                       | MulOU of 't * 't * 't
                       | Mult of 't * 't
                       | MultU of 't * 't

                       | Neg of 't * 't
                       | NegU of 't * 't

                       | Nor of 't * 't * 't

                       | Not of 't * 't

                       | Or of 't * 't * 't
                       | OrI of 't * 't * 't

                       | Rem of 't * 't * 't
                       | RemU of 't * 't * 't

                       | Rol of 't * 't * 't
                       | Ror of 't * 't * 't

                       | Sll of 't * 't * 't
                       | SllV of 't * 't * 't
                       | Sra of 't * 't * 't
                       | SraV of 't * 't * 't
                       | Srl of 't * 't * 't
                       | SrlV of 't * 't * 't

                       | Sub of 't * 't * 't
                       | SubU of 't * 't * 't

                       | Xor of 't * 't * 't
                       | XorI of 't * 't * 't

                       (* Comparison Instructions *)

                       | Seq of 't * 't * 't

                       | Sge of 't * 't * 't
                       | SgeU of 't * 't * 't

                       | Sgt of 't * 't * 't
                       | SgtU of 't * 't * 't

                       | Sle of 't * 't * 't
                       | SleU of 't * 't * 't

                       | Slt of 't * 't * 't
                       | SltI of 't * 't * 't
                       | SltU of 't * 't * 't
                       | SltIU of 't * 't * 't

                       | Sne of 't * 't * 't

                       (* Load and Store Instructions *)

                       | La of 't * 'l

                       | Lb of 't * 'l
                       | LbU of 't * 'l

                       | Ld of 't * 'l

                       | Lh of 't * 'l
                       | LhU of 't * 'l

                       | Lw of 't * 'l

                       | LwCZ of 't * 'l

                       | LwL of 't * 'l
                       | LwR of 't * 'l

                       | Sb of 't * 'l

                       | Sd of 't * 'l

                       | Sh of 't * 'l

                       | Sw of 't * 'l

                       | SwCZ of 't * 'l

                       | SwL of 't * 'l
                       | SwR of 't * 'l

                       | Ulh of 't * 'l
                       | UlhU of 't * 'l

                       | Ulw of 't * 'l

                       | Ush of 't * 'l

                       | Usw of 't * 'l

                       (* Exception and Trap Instruction *)

                       | Rfe
                       | Syscall
                       | Break
                       | Nop

                       (* Constant-Manipulating Instructions *)

                       | Li of 't * 't
                       | Lui of 't * 't

                       (* Branch and Jump Instructions *)

                       | B of 'l

                       | Bczt of 'l
                       | Bczf of 'l

                       | Beq of 't * 't * 'l

                       | Beqz of 't * 'l

                       | Bge of 't * 't * 'l
                       | BgeU of 't * 't * 'l

                       | Begz of 't * 'l

                       | Bgezal of 't * 'l

                       | Bgt of 't * 't * 'l
                       | BgtU of 't * 't * 'l

                       | Bgtz of 't * 'l

                       | Ble of 't * 't * 'l
                       | BleU of 't * 't * 'l

                       | Blez of 't * 'l

                       | Bltzal of 't * 'l

                       | Blt of 't * 't * 'l
                       | BltU of 't * 't * 'l

                       | Bltz of 't * 'l

                       | Bne of 't * 't * 'l

                       | Bnez of 't * 'l

                       | J of 'l

                       | Jal of 'l
                       | Jalr of 't

                       | Jr of 't

                       (* Data Movement Instructions *)

                       | Mfhi of 't
                       | Mflo of 't

                       | Mthi of 't
                       | Mtlo of 't

                       | Mfcz of 't * 't

                       | Mtcz of 't * 't

datatype Label = LUser of string
               | LTemp of int

end
