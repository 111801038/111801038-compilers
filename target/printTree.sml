structure PrintTree = struct

fun binOpr opr = case opr of
                  Tree.PLUS => print("Tree.PLUS")
                | Tree.MINUS => print("Tree.MINUS")
                | Tree.MUL => print("Tree.MUL")
                | Tree.DIV => print("Tree.DIV")
                | Tree.AND => print("Tree.AND")
                | Tree.OR => print("Tree.OR")
                | Tree.XOR => print("Tree.XOR")
                | Tree.LSHIFT => print("Tree.LSHIFT")
                | Tree.RSHIFT => print("Tree.RSHIFT")
                | Tree.ARSHIFT => print("Tree.ARSHIFT")

fun relOpr opr = case opr of
                Tree.LT => print("Tree.LT")
              | Tree.GT => print("Tree.GT")
              | Tree.GE => print("Tree.GE")
              | Tree.LE => print("Tree.LE")
              | Tree.EQ => print("Tree.EQ")
              | Tree.NE => print("Tree.NE")
              | Tree.ULT => print("Tree.ULT")
              | Tree.ULE => print("Tree.ULE")
              | Tree.UGT => print("Tree.UGT")
              | Tree.UGE => print("Tree.UGE")

fun labelList [] = ()
  | labelList [x] = print(Int.toString x)
  | labelList (x :: xs) = ( print(Int.toString x); print(", "); labelList xs )

fun seqHandler e =
  case e of
    Tree.MOVE(e1, e2) => (print("Tree.MOVE(");
                          compile e1;
                          print ", ";
                          compile e2;
                          print(")"))
  | Tree.EXP(e) => (print("Tree.EXP(");
                    compile e;
                    print(")")) 
  | Tree.JUMP(e,labels) => (print("Tree.JUMP(");
                            compile e;
                            print ", [";
                            labelList labels;
                            print "])")
  | Tree.CJUMP(rel, e1, e2, l1, l2) => (print "Tree.CJUMP(";
                                        relOpr rel; print ", ";
                                        compile e1; print ", ";
                                        compile e2; print (", " ^ (Int.toString l1) ^ ", " ^ (Int.toString l2) ^ ")"))
  | Tree.SEQ(s1, s2) => ( print "Tree.SEQ(";
                          seqHandler s1;
                          print ", ";
                          seqHandler s2;
                          print ")" )
  | Tree.LABEL l => print ("Tree.LABEL(" ^ (Int.toString l) ^ ")") 


and compile e = 
    case e of
          Tree.CONST x => print("Tree.CONST(" ^ (Int.toString x) ^ ")")
        | Tree.BINOP( opr, x, y) => (print("Tree.BINOP("); (binOpr opr); print ", " ; (compile x); print ", "; (compile y); print ")" )
        | Tree.ESEQ(s, e) => (print "Tree.ESEQ("; seqHandler s; print ", "; compile e; print ")")
        | Tree.NAME n => print( "Tree.NAME(" ^ (Int.toString n) ^ ")")
        | Tree.TEMP t => print("Tree.TEMP(" ^ (Int.toString t) ^ ")")
        | Tree.MEM e => ( print("Tree.MEM("); compile e; print ")")
        | Tree.CALL (x, xs) => (print("Tree.CALL("); compile x; print ", ["; expListHandler xs; print "])")

and expListHandler [] = ()
  | expListHandler [x] = compile x
  | expListHandler (x :: xs) = (compile x; print ", "; expListHandler xs)

and stmtListCompile [] = ()
  | stmtListCompile [x] = seqHandler x
  | stmtListCompile (x :: xs) = (seqHandler x; print "\n"; stmtListCompile xs)

and framePrint _ = (
                      print ("=======Registers==========\n");
                      print ("Stack Pointer SP = TEMP(" ^ (Int.toString MipsFrame.SP) ^ ")");
                      print ("\nFrame Pointer FP = TEMP(" ^ (Int.toString MipsFrame.FP) ^ ")");
                      print ("\nReturn Address RA = TEMP(" ^ (Int.toString MipsFrame.RA) ^ ")");
                      print ("\nReturn Value RV = TEMP(" ^ (Int.toString MipsFrame.RV) ^ ")");
                      print "\n"
)

end