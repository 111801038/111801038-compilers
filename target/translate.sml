structure Translate = struct

exception SyntaxError of string
exception NotFound

val n = Tree.CONST 0

val sp = Tree.TEMP(MipsFrame.SP)
val fp = Tree.TEMP(MipsFrame.FP)
val ra = Tree.TEMP(MipsFrame.RA)
val rv = Tree.TEMP(MipsFrame.RV)

fun getLast [x] = (nil, x) 
    | getLast(x :: xs) = (rev xs, x)
    | getLast _ = raise SyntaxError "Something went wrong"

fun formSeq [] = Tree.EXP(n)
    | formSeq [x] = x
    | formSeq (x::xs) = Tree.SEQ(x, formSeq(xs))


fun relopHandler(opr) =
    case opr of
      Tiger.L => Tree.LT
    | Tiger.G => Tree.GT
    | Tiger.GE => Tree.GE
    | Tiger.LE => Tree.LE
    | Tiger.NEQ => Tree.NE
    | Tiger.EQ => Tree.EQ
    | _ => raise SyntaxError "Unexpected operator"


fun binopHandler x opr y =
    case (opr, x, y) of
      (Tiger.ADD, _, _) => Tree.BINOP(Tree.PLUS, x, y)
    | (Tiger.SUB, _, _) => Tree.BINOP(Tree.MINUS, x, y)
    | (Tiger.MUL, _, _) => Tree.BINOP(Tree.MUL, x, y)
    | (Tiger.DIV, _, _) => Tree.BINOP(Tree.DIV, x, y)
    | (Tiger.AND, _, _) =>Tree.BINOP(Tree.AND, x, y)
    | (Tiger.OR, _, _) => Tree.BINOP(Tree.OR, x, y)
    | (Tiger.L, Tree.CONST x, Tree.CONST y) => if (x < y) then (Tree.CONST 1) else (n)
    | (Tiger.LE, Tree.CONST x, Tree.CONST y) => if (x <= y) then (Tree.CONST 1) else (n)
    | (Tiger.G, Tree.CONST x, Tree.CONST y) => if (x > y) then (Tree.CONST 1) else (n)
    | (Tiger.GE, Tree.CONST x, Tree.CONST y) => if (x >= y) then (Tree.CONST 1) else (n)
    | (Tiger.EQ, Tree.CONST x, Tree.CONST y) => if (x = y) then (Tree.CONST 1) else (n)
    | (Tiger.NEQ, Tree.CONST x, Tree.CONST y) => if (x <> y) then (Tree.CONST 1) else (n)
    | _ => let 
                val ret = Temp.newtemp() 
                val trueLabel = Temp.newlabel()
                val falseLabel = Temp.newlabel() 
            in
                Tree.ESEQ(formSeq([Tree.MOVE(Tree.TEMP ret, n), 
                                Tree.CJUMP(relopHandler(opr), x, y, trueLabel, falseLabel), 
                                Tree.LABEL trueLabel, 
                                Tree.MOVE(Tree.TEMP ret, Tree.CONST 1), 
                                Tree.LABEL falseLabel]), 
                        Tree.TEMP ret) 
            end 

fun ifHandler(cond, body) =
  let
    val trueLabel = Temp.newlabel()
    val falseLabel = Temp.newlabel()
  in
    Tree.ESEQ(formSeq([ Tree.CJUMP(Tree.EQ, Tree.CONST 1, cond, trueLabel, falseLabel),
                        Tree.LABEL(trueLabel),
                        Tree.EXP(body),
                        Tree.LABEL(falseLabel)
                      ]),
                      n 
              )
  end

fun ifElseHandler(cond, body, alt) =
  let
    val trueLabel = Temp.newlabel()
    val falseLabel = Temp.newlabel()
    val endLabel = Temp.newlabel()
  in
    Tree.ESEQ(formSeq([ Tree.CJUMP(Tree.EQ, Tree.CONST 1, cond, trueLabel, falseLabel),
                        Tree.LABEL(trueLabel),
                        Tree.EXP(body),
                        Tree.JUMP(Tree.NAME endLabel, [endLabel]),
                        Tree.LABEL(falseLabel),
                        Tree.EXP(alt),
                        Tree.LABEL(endLabel)
                      ]),
                      n 
              )
  end

fun whileHandler(cond, body, endLabel) =
  let
    val startLabel = Temp.newlabel()
    val bodyLabel = Temp.newlabel()
  in
    Tree.ESEQ(formSeq([ Tree.LABEL(startLabel),
                        Tree.CJUMP(Tree.EQ, Tree.CONST 1, cond, bodyLabel, endLabel),
                        Tree.LABEL(bodyLabel),
                        Tree.EXP(body),
                        Tree.JUMP(Tree.NAME startLabel, [startLabel]),
                        Tree.LABEL(endLabel)
                      ]),
                      n 
              )
  end

fun forHandler(var, from, to, body, breakLabel) =
        let
            val bodyLabel = Temp.newlabel()
            val updateLabel = Temp.newlabel()
        in
            Tree.ESEQ(formSeq([Tree.MOVE(var, from),
                              Tree.LABEL(updateLabel),
                              Tree.CJUMP(Tree.LE, var, to, bodyLabel, breakLabel),
                              Tree.LABEL(bodyLabel),
                              Tree.EXP(body),
                              Tree.MOVE(var, Tree.BINOP(Tree.PLUS, var, Tree.CONST 1)),
                              Tree.JUMP(Tree.NAME(updateLabel), [updateLabel]),
                              Tree.LABEL(breakLabel)]),
                              n
            )
        end


fun fieldsHandler(env, fenv, [Tiger.TyField{name, ty}], i) = (
                                                      HashTable.insert env (name, Temp.newtemp());
                                                      [Tree.MOVE(Tree.TEMP(Temp.curtemp()),
                                                       Tree.MEM(Tree.BINOP( Tree.PLUS,
                                                       sp,
                                                       Tree.CONST i)))]
                                                    )
  | fieldsHandler(env, fenv, (x :: xs), i) =  fieldsHandler(env, fenv, [x], i) @ fieldsHandler(env, fenv, xs, i + 4)
  | fieldsHandler(env, fenv, _, i) = [Tree.EXP(n)]

fun funDecHandler(env, fenv, name, fields, body, breakLabel) =  formSeq([ Tree.MOVE( sp, Tree.BINOP(Tree.MINUS, sp, Tree.CONST 4)),
                                                                          Tree.MOVE(Tree.MEM(sp), fp),
                                                                          Tree.MOVE( sp, Tree.BINOP(Tree.MINUS, sp, Tree.CONST 4)),
                                                                          Tree.MOVE(Tree.MEM(sp), ra),
                                                                          Tree.MOVE( fp, Tree.BINOP(Tree.PLUS, sp, Tree.CONST 8)),
                                                                          Tree.SEQ( formSeq(fieldsHandler(env, fenv, fields, 0)),
                                                                                  Tree.MOVE( Tree.MEM(rv),
                                                                                  compile env fenv body breakLabel)),
                                                                          Tree.JUMP(Tree.MEM(ra), [])])


and decHandler(dec, env, fenv, breakLabel) =
  case dec of
      Tiger.VarDec{name, ty, init} => (
                                        HashTable.insert env (name, Temp.newtemp());
                                        Tree.MOVE(Tree.TEMP (Temp.curtemp()), compile env fenv init breakLabel)
                                      )
    | Tiger.FuncDec{name, fields, ty, body} => (
                                                  HashTable.insert fenv (name, Temp.newlabel());
                                                  formSeq([Tree.LABEL(Temp.curlabel()),
                                                          funDecHandler(env, fenv, name, fields, body, breakLabel),
                                                          Tree.LABEL(Temp.newlabel())]) 
                                                )
    | _ => Tree.EXP(n)

and decListHandler(decs, env, fenv, breakLabel) =
  case decs of
      [] => [Tree.EXP(n)]
    | [x] => [decHandler(x, env, fenv, breakLabel)]
    | (x :: xs) => [decHandler(x, env, fenv, breakLabel)] @ decListHandler(xs, env, fenv, breakLabel)   

and letHandler(decs, body, env, fenv, breakLabel) = Tree.ESEQ( Tree.SEQ(formSeq(decListHandler(decs, env, fenv, breakLabel)),
                                                               formSeq(expListHandler(body, env, fenv, breakLabel))), n) 

and expListHandler(exps, env, fenv, breakLabel) =
  case exps of
      [] => [Tree.EXP(n)]
    | [x] => [Tree.EXP(compile env fenv x breakLabel)]
    | (x :: xs) => [Tree.EXP(compile env fenv x breakLabel)] @ expListHandler(xs, env, fenv, breakLabel)



and varHandler(env, fenv, x) =
    case x of
      Tiger.V x => (case (HashTable.find env x) of
                          SOME v => Tree.TEMP v
                        | NONE => (TextIO.output(TextIO.stdErr, "variable " ^ x ^ " not found\n"); OS.Process.exit OS.Process.failure))
    | _ => n

and brHandler(env, fenv, exps, breakLabel) =  let val (xs, x) = getLast (rev exps)
                      in
                        Tree.ESEQ( formSeq(expListHandler(xs, env, fenv, breakLabel)), compile env fenv x breakLabel)
                      end

and argHandler(env, fenv, [], breakLabel) = []
  | argHandler(env, fenv, [x], breakLabel) = [compile env fenv x breakLabel]
  | argHandler(env, fenv, (x :: xs), breakLabel) = argHandler(env, fenv, [x], breakLabel) @ argHandler(env, fenv, xs, breakLabel)

and ifHelper(env, fenv, breakLabel, cond, body, alt) =  (case alt of
                                      SOME e => ifElseHandler(compile env fenv cond breakLabel,
                                                              compile env fenv body breakLabel,
                                                              compile env fenv e breakLabel)

                                      | NONE => ifHandler(compile env fenv cond breakLabel,
                                                          compile env fenv body breakLabel))

and letHelper(env, fenv, breakLabel, decs, body) = let
                                                      val envTemp = HashTable.copy env
                                                      val fenvTemp = HashTable.copy fenv
                                                    in
                                                      letHandler(decs, body, envTemp, fenvTemp, breakLabel)
                                                    end

and forHelper(env, fenv, breakLabel, var, from, to, body) =
  let
   val envTemp = HashTable.copy env
   val fenvTemp = HashTable.copy fenv
  in
    ( HashTable.insert env (var, Temp.newtemp());
      forHandler( Tree.TEMP (Temp.curtemp()),
                  compile env fenv from breakLabel,
                  compile env fenv to breakLabel,
                  compile env fenv body (Temp.newlabel()),
                  Temp.curlabel()
                )
    )
  end

and funCallHelper(env, fenv, breakLabel, func, args) =
  let
    val name = HashTable.find fenv func
  in
    case name of 
      SOME f => Tree.CALL(Tree.NAME f, argHandler(env, fenv, args, breakLabel))
    | NONE => (TextIO.output(TextIO.stdErr, "function " ^ func ^ " not found\n"); OS.Process.exit OS.Process.failure)
  end 

and compile env fenv e breakLabel=
    case e of
      Tiger.Int x => Tree.CONST x
    | Tiger.Opr {x, opr, y} => (binopHandler (compile env fenv x breakLabel) opr (compile env fenv y breakLabel))
    
    | Tiger.Neg e => (binopHandler (n) Tiger.SUB (compile env fenv e breakLabel))
    
    | Tiger.If{cond, body, alt} => ifHelper(env, fenv, breakLabel, cond, body, alt)
    
    | Tiger.While{cond, body} => whileHandler(compile env fenv cond breakLabel,
                                              compile env fenv body (Temp.newlabel()),
                                              Temp.curlabel())
    
    | Tiger.Break => Tree.ESEQ(Tree.JUMP (Tree.NAME (breakLabel), [breakLabel]), n)
    
    | Tiger.Let{decs, body} => letHelper(env, fenv, breakLabel, decs, body)
    
    | Tiger.For{var, from, to, body} => forHelper(env, fenv, breakLabel, var, from, to, body) 
    
    | Tiger.Ass{x, y} => Tree.ESEQ( Tree.MOVE( varHandler(env, fenv, x),
                                               compile env fenv y breakLabel),
                                    n)
    | Tiger.Var x => varHandler(env, fenv, x)
    
    | Tiger.Nil => n
    
    | Tiger.Br exps => brHandler(env, fenv, exps, breakLabel)
    
    | Tiger.FunCall{func, args} => funCallHelper(env, fenv, breakLabel, func, args)
    
    | _ => n

fun help (Tiger.E e) = compile (HashTable.mkTable (HashString.hashString, op=) (100, NotFound)) (HashTable.mkTable (HashString.hashString, op=) (100, NotFound)) e ~1

end