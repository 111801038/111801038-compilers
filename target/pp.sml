structure PP : 
     sig val compile : Tiger.prog -> string end =
struct 

structure A = Tiger

val out = ref ""

fun yellow s = (str(chr(27)) ^ "[33m" ^ s ^ str(chr(27)) ^ "[0m")

fun green s = (str(chr(27)) ^ "[32m" ^ s ^ str(chr(27)) ^ "[0m")

fun red s = (str(chr(27)) ^ "[31m" ^ s ^ str(chr(27)) ^ "[0m")


fun compile e0 =
 let fun say s =  out := !out ^ s  
  fun sayln s= (say s; say "\n") 

  fun indent 0 = ()
    | indent i = (say "  "; indent(i-1))

  fun opname A.ADD = yellow "+"
    | opname A.SUB = yellow "-"
    | opname A.MUL = yellow "*"
    | opname A.DIV = yellow "/"
    | opname A.EQ =  yellow "="
    | opname A.NEQ = yellow "<>"
    | opname A.L = yellow "<"
    | opname A.LE = yellow "<="
    | opname A.G = yellow ">"
    | opname A.GE = yellow ">="
    | opname A.AND = yellow "&"
    | opname A.OR = yellow "|"

  fun doExps d f [a] = (indent (d+1); f(a,d+1); sayln "")
    | doExps d f (a::r) = (indent (d+1); f(a,d+1); sayln "; "; doExps d f r )
    | doExps d f nil = ()

  fun doPara d f [a] = (f(a,d+1))
    | doPara d f (a::r) = (say ""; f(a,d+1); say ", "; doPara d f r)
    | doPara d f nil = ()

  fun doDecs d f [a] = (indent (d+1); f(a,d+1))
    | doDecs d f (a::r) = (indent (d+1); f(a,d+1); doDecs d f r)
    | doDecs d f nil = ()


  fun linit(A.V(s),d) = (
                          say(s)
                        )

    | linit(A.B(x,e),d) = (
                            say "";
                            linit(x, d);
                            say "[";
                            exp(e, d);
                            say "]"
                          )
    
    | linit(A.D(x,e),d) = (
                            linit(x, d);
                            say ".";
                            say(e)
                          )

  and exp(A.Var x, d) = linit(x,d)

    | exp(A.Nil, d) = (
                        say (yellow "nil")
                      )

    | exp(A.Int i, d) = (
                          say (red (Int.toString i))
                        )

    | exp(A.Str(s),d) = (
                          say (red ("\"" ^ concat( map Char.toString (explode s) )^ "\""))
                        )

    | exp(A.FunCall{args, func},d) =  (
                                        say(func);
                                        say "(";
                                        doPara d exp args;
                                        say ")"
                                      )

    | exp(A.Array{ty, init, size},d) =  (
                                          say (green ty);
                                          say "[";
                                          exp(size, d); say "]";
                                          say (yellow " of ");
                                          exp(init, d)
                                        )


    | exp(A.Record{field,ty},d) = let
                                    fun f((name,e),d) = (
                                                          say(name);
                                                          say (yellow " = ");
                                                          exp(e,d)
                                                        )
	                                in 
                                    say(ty);
                                    say " { ";
                                    doPara 0 f field;
                                    say "}" 
	                                end

    | exp(A.Neg(e), d) =  (
                            say "-";
                            exp(e, d)
                          )

    | exp(A.Opr{x,opr,y},d) = (
                                exp(x, 0);
                                say " ";
                                say(opname opr);
                                say " "; exp(y, d)
                              )

    | exp(A.Br l, d) =  (
                          sayln "(";
                          doExps d exp l;
                          indent d;
                          say ")"
                        )

    | exp(A.Ass{x,y},d) = (
                            linit(x,0);
                            say (yellow " := ");
                            exp(y,d)
                          )

    | exp(A.If{cond,body,alt},d) =  (
                                      say (yellow "if ");
                                      exp(cond, d);
                                      sayln (yellow " then ");
                                      indent (d+1); exp(body, d+1);
                                      case alt of NONE => ()
                                                | SOME e => (
                                                              sayln "";
                                                              indent d;
                                                              sayln (yellow "else");
                                                              indent (d+1); exp(e,d+1)
                                                            )
                                    )

    | exp(A.While{cond,body},d) = (
                                    say (yellow "while ");
                                    exp(cond,d);
                                    sayln (yellow " do ");
                                    indent (d+1); exp( body,d+1)
                                  )

    | exp(A.For{var=x,from=lo,to=hi,body=body},d) = (
                                                      say (yellow "for ");
                                                      say(x);
                                                      say (yellow " := ");
                                                      exp(lo,d);
                                                      say (yellow " to ");
                                                      exp(hi,d);
                                                      sayln (yellow " do");
                                                      indent (d+1); exp(body,d+1)
                                                    )

    | exp(A.Break, d) = ( 
                          say (yellow "break")
                        )

    | exp(A.Let{decs,body},d) = (
                                  sayln (yellow "let");
                                  doDecs d dec decs;
                                  indent d;
                                  sayln (yellow "in");
                                  doExps d exp body;
                                  indent d;
                                  say (yellow "end")
                                )

  and dec(A.TypeDec l, d) = let 
                              fun tdec({name, ty}) =  (
                                                        say (yellow "type ");
                                                        say(name);
                                                        say (yellow " = ");
                                                        typ(ty,d)
                                                      )  
                            in 
                              tdec(l);
                              sayln ""
                            end

    | dec(A.VarDec{name,ty,init},d) = (
                                        say (yellow "var ");
                                        say(name);
                                        case ty of NONE => () 
                                                | SOME(s)=> (
                                                              say " : ";
                                                              say(s)
                                                            );
                                        say (yellow " := ");
                                        exp(init,d);
                                        sayln ""
                                      )

    | dec(A.FuncDec l, d) = let
                              fun f({name,fields,ty,body},d) =  (
                                                                  say (name);
                                                                  say "(";
                                                                  doPara d tyfields fields;
                                                                  say ")";
		                                                              case ty of NONE => say ""
                                                                          | SOME(s) =>  (
                                                                                          say " : ";
                                                                                          say (green s)
                                                                                        );
		                                                              say (yellow " = ");
                                                                  (* indent (d+1); *)
                                                                  exp(body, d+1)
                                                                )
	                          in
                              say (yellow "function ");
                              f(l, d);
                              sayln ""
	                          end

    | dec(A.PrimDec{name, fields,ty},d) = (
                                            say (yellow "primitive ");
                                            say (name);
                                            say "(";
                                            doPara d tyfields fields;
                                            say ")";
                                            case ty of NONE => ()
                                                    | SOME(s) =>  (
                                                                    say " : ";
                                                                    say (green s)
                                                                  );
                                            sayln ""
                                          )



 and  typ(A.AliasTy(s), d) =  (
                                say (green s)
                              )

    | typ(A.RecTy l, d) = (
                            say "{ ";
                            doPara d tyfields l;
                            say " }"
                          )
		
    | typ(A.ArrTy(s),d) = (
                            say (yellow "array of ");
                            say (green s)
                          )

 and tyfields(A.TyField l , d) =  let
                                    fun f({name,ty},d) =  (
                                                            say (str(chr(27)) ^ "[0m" ^ name ^ str(chr(27)) ^ "[0m");
                                                            say " : "; say (green ty)
                                                          )
                                  in 
                                    f(l,d)
                                  end

 and program(A.E e, d) = (exp(e, d))  
 
 in  program(e0,0); sayln ""; !out

 end                             

end
